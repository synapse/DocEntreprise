\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{synapseTemplate}[2016/09/25 synapse LaTeX class]


\newcommand{\headlinecolor}{\normalcolor}
\LoadClass{article}
\RequirePackage{xcolor}
%\RequirePackage[francais]{babel}
\definecolor{bkgColor}{HTML}{C44D43}


\DeclareOption{green}{\renewcommand{\headlinecolor}{\color{green}}}
\DeclareOption{red}{\renewcommand{\headlinecolor}{\color{bkgColor}}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax

\renewcommand{\maketitle}{%
%  \twocolumn[%
    %% \fontsize{50}{60}\fontfamily{phv}\fontseries{b}%
    %% \fontshape{sl}\selectfont\headlinecolor
    %% \@title
    %% \medskip
  %% ]%
}

\renewcommand{\section}{%
  \@startsection
      {section}{1}{0pt}{-1.5ex plus -1ex minus -.2ex}%
      {1ex plus .2ex}{\large\sffamily\slshape\headlinecolor}%
}

%% \renewcommand{\normalsize}{\fontsize{9}{10}\selectfont}
%% \setlength{\textwidth}{17.5cm}
%% \setlength{\textheight}{22cm}
%% \setcounter{secnumdepth}{0}


%% \newcommand{\headlinecolor}{\normalcolor}
%% \LoadClass{article}
%% \RequirePackage{xcolor}


%% \DeclareOption{red}{\renewcommand{\headlinecolor}{\color{bkgColor}}}
%% \DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
%% \ProcessOptions\relax

%% \RequirePackage[utf8]{inputenc}
%% \RequirePackage[T1]{fontenc}


%% \RequirePackage{hyperref}
%% \hypersetup{
%%   colorlinks=false, %set true if you want colored links
%%   linktoc=all,     %set to all if you want both sections and subsections linked
%%   linkcolor=blue,  %choose some color if you want links to stand out
%% }


